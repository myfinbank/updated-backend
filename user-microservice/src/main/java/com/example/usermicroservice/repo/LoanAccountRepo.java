package com.example.usermicroservice.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.usermicroservice.entity.LoanAccountEntity;
import com.example.usermicroservice.entity.LoanRequestEntity;

public interface LoanAccountRepo extends JpaRepository<LoanAccountEntity,Integer>{
	
}
