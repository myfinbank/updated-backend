package com.example.usermicroservice.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.usermicroservice.entity.WithdrawRequestEntity;

public interface WithdrawRequestRepo extends JpaRepository<WithdrawRequestEntity,Long>{

	List<WithdrawRequestEntity> findAllByIsApprovedFalse();

}
