package com.example.usermicroservice.repo;


import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.utilities.ACCOUNTTYPE;

public interface AccountRepo extends JpaRepository<AccountEntity,Long>{

	Optional<AccountEntity> findByAccountId(long accountId);

	AccountEntity findByAccountNumber(String accountNumber);

	AccountEntity findByUserId(long userId);
	
	List<AccountEntity> findAllByUserId(long userId);

	AccountEntity findByUserIdAndType(long userId, ACCOUNTTYPE type);
	
	@Modifying
    @Query("UPDATE AccountEntity a SET a.amountAvailable = :amountAvailable WHERE a.accountNumber = :accountNumber")
    void updateAmountAvailable(@Param("accountNumber") String accountNumber, @Param("amountAvailable") double amountAvailable);

	void deleteAllByUserId(long userId);

}
