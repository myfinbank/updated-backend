package com.example.usermicroservice.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.usermicroservice.entity.TransactionEntity;

public interface TransactionRepo extends JpaRepository<TransactionEntity, Long>{

	List<TransactionEntity> findByUserId(long userId);
	
	@Query("SELECT t FROM TransactionEntity t WHERE t.transactorAccountId = :accountNumber OR t.transacteeAccountId = :accountNumber")
    List<TransactionEntity> findByAccountNumber(@Param("accountNumber") String accountNumber);

}
