package com.example.usermicroservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.usermicroservice.entity.DepositeAccountEntity;

public interface DepositeAccountRepo extends JpaRepository<DepositeAccountEntity, Integer> {

}
