package com.example.usermicroservice.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.usermicroservice.entity.DepositRequestEntity;

public interface DepositRequestRepo extends JpaRepository<DepositRequestEntity,Long>{

	List<DepositRequestEntity> findAllByIsApprovedFalse();

	

}
