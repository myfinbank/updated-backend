package com.example.usermicroservice.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.usermicroservice.entity.UserAccountRequestEntity;

public interface UserAccountRequestRepo extends JpaRepository<UserAccountRequestEntity, Long> {

	List<UserAccountRequestEntity> findById(long arequestId);
	List<UserAccountRequestEntity> findAllByIsApprovedFalse();

}
