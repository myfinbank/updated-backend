package com.example.usermicroservice.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.usermicroservice.entity.LoanRequestEntity;

public interface LoanRequestRepo extends JpaRepository<LoanRequestEntity, Long>{
	List<LoanRequestEntity> findAllByIsApprovedFalse();
}
