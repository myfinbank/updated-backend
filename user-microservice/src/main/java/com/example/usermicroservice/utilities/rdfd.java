package com.example.usermicroservice.utilities;

public class rdfd {
	private long userId;
	private double requestedAmount;
	private int tenure;
    private FDRD type;
    
    
	public rdfd() {
		super();
		// TODO Auto-generated constructor stub
	}
	public rdfd(long userId, double requestedAmount, int tenure, FDRD type) {
		super();
		this.userId = userId;
		this.requestedAmount = requestedAmount;
		this.tenure = tenure;
		this.type = type;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public double getRequestedAmount() {
		return requestedAmount;
	}
	public void setRequestedAmount(double requestedAmount) {
		this.requestedAmount = requestedAmount;
	}
	public int getTenure() {
		return tenure;
	}
	public void setTenure(int tenure) {
		this.tenure = tenure;
	}
	public FDRD getType() {
		return type;
	}
	public void setType(FDRD type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "rdfd [userId=" + userId + ", requestedAmount=" + requestedAmount + ", tenure=" + tenure + ", type="
				+ type + "]";
	}
    
    
}
