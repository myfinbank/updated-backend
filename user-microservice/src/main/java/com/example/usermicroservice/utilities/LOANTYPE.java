package com.example.usermicroservice.utilities;

public enum LOANTYPE {
	PERSONALLOAN,
	GOLDLOAN,
	CARLOAN,
	EDUCATIONLOAN,
	HOMELOAN
}
