package com.example.usermicroservice.utilities;

public enum FDRD {
	REGULARFIXED,
	SENIORCITIZEN,
	TAXSAVER,
	REGULARRECURRING
}
