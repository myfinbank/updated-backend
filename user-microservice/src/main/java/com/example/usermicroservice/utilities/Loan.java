package com.example.usermicroservice.utilities;
public class Loan {
	private long userId;
	private double requestedAmount;
	private int tenure;
    private LOANTYPE type;
    
    public Loan() {
    	super();
    	// TODO Auto-generated constructor stub
    }

	public Loan(long userId, double requestedAmount, int tenure, LOANTYPE type) {
		super();
		this.userId = userId;
		this.requestedAmount = requestedAmount;
		this.tenure = tenure;
		this.type = type;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public double getRequestedAmount() {
		return requestedAmount;
	}

	public void setRequestedAmount(double requestedAmount) {
		this.requestedAmount = requestedAmount;
	}

	public int getTenure() {
		return tenure;
	}

	public void setTenure(int tenure) {
		this.tenure = tenure;
	}

	public LOANTYPE getType() {
		return type;
	}

	public void setType(LOANTYPE type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Loan [userId=" + userId + ", requestedAmount=" + requestedAmount + ", tenure=" + tenure + ", type="
				+ type + "]";
	}

   

   
}
