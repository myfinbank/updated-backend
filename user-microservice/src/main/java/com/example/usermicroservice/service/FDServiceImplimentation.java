package com.example.usermicroservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.FixedDepositEntity;
import com.example.usermicroservice.repo.AccountRepo;
import com.example.usermicroservice.repo.FixedDepositRepository;

@Service
public class FDServiceImplimentation implements FixedDepositService {
	@Autowired
	private AccountRepo accountRepository;
	@Autowired
    private FixedDepositRepository fixedDepositRepository;

	@Override
	public AccountEntity getAccountByAccountNumber(String accountNumber) {
		return accountRepository.findByAccountNumber(accountNumber);
	}

	@Override
	public void updateAccountBalance(String accountNumber, double amount) {
		AccountEntity account = accountRepository.findByAccountNumber(accountNumber);
        account.setAmountAvailable(account.getAmountAvailable() - amount);
        accountRepository.save(account);
	}

	@Override
	public FixedDepositEntity createFixedDeposit(String accountNumber, double principalAmount, double interestRate,
			int durationInMonths) {
		FixedDepositEntity fixedDeposit = new FixedDepositEntity(accountNumber, principalAmount, interestRate, durationInMonths);
        fixedDepositRepository.save(fixedDeposit);
        return fixedDeposit;
	}

	@Override
	public double calculateMaturityAmount(String accountNumber) {
		// Logic to calculate maturity amount
        FixedDepositEntity fixedDeposit = fixedDepositRepository.findByAccountNumber(accountNumber);
        double principalAmount = fixedDeposit.getPrincipalAmount();
        double interestRate = fixedDeposit.getInterestRate();
        int durationInMonths = fixedDeposit.getDurationInMonths();
        // Perform the necessary calculations to determine the maturity amount
        double maturityAmount = principalAmount * Math.pow(1 + interestRate / 100, durationInMonths / 12.0);
        return maturityAmount;
	}
	
}
