package com.example.usermicroservice.service;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.RecurringDeposite;

public interface RecurringDepositeService {
	public AccountEntity getAccountByAccountNumber(String accountNumber);

	 public void updateAccountBalance(String accountNumber, double amount) ;

	public RecurringDeposite createRecurringDeposit(String accountNumber, double amount, double interestRate);

	public double calculateTotalSavings(long userId);

}
