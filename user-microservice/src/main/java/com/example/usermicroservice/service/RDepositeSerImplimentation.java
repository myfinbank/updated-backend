package com.example.usermicroservice.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.RecurringDeposite;
import com.example.usermicroservice.repo.AccountRepo;
import com.example.usermicroservice.repo.RecurringDepositeRepo;

@Service
public class RDepositeSerImplimentation implements RecurringDepositeService {
	@Autowired
    AccountRepo accountRepository;
	@Autowired
    RecurringDepositeRepo recurringDepositRepository;

	@Override
    public AccountEntity getAccountByAccountNumber(String accountNumber) {
        return accountRepository.findByAccountNumber(accountNumber);
    }

    @Override
    public void updateAccountBalance(String accountNumber, double amount) {
        AccountEntity account = accountRepository.findByAccountNumber(accountNumber);
        account.setAmountAvailable(account.getAmountAvailable() - amount);
        accountRepository.save(account);
    }

    @Override
    public RecurringDeposite createRecurringDeposit(String accountNumber, double amount, double interestRate) {
        RecurringDeposite recurringDeposit = new RecurringDeposite(accountNumber, amount, interestRate);
        recurringDepositRepository.save(recurringDeposit);
        return recurringDeposit;
    }

    @Override
    public double calculateTotalSavings(long userId) {
        AccountEntity account = accountRepository.findByUserId(userId);
        double totalSavings = account.getAmountAvailable();
        List<RecurringDeposite> recurringDeposits = recurringDepositRepository.findByUserId(userId);
        for (RecurringDeposite rd : recurringDeposits) {
            totalSavings += rd.getTotalAmount() + (rd.getTotalAmount() * rd.getInterestRate() / 100);
        }
        return totalSavings;
    }
}

