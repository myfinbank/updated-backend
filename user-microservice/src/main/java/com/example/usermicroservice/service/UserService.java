package com.example.usermicroservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.UserEntity;
import com.example.usermicroservice.repo.AccountRepo;
import com.example.usermicroservice.repo.UserRepo;
import com.example.usermicroservice.utilities.UserAccounts;
import com.example.usermicroservice.utilities.UserList;
import com.example.usermicroservice.utilities.UserProfile;

@Service
public class UserService {
	
	@Autowired
	UserRepo repo;
	
	@Autowired
	AccountRepo arepo;

	public List<UserList> getAllUsers() {
		List<UserEntity> userlist = repo.findAll();
		List<UserList> users = new ArrayList<>();
		for(UserEntity user: userlist) {
			users.add(new UserList(user.getUserId(),user.getUsername(),user.getEmail()));
		}
		return users;
	}

	public List<UserProfile> getUserProfiles() {
		List<UserEntity> userlist = repo.findAll();
		List<UserProfile> userProfiles = new ArrayList<>();
		for(UserEntity user:userlist) {
			List<AccountEntity> accountlist = arepo.findAllByUserId(user.getUserId());
			List<UserAccounts> userAccounts = new ArrayList<>();
			for(AccountEntity account: accountlist) {
				userAccounts.add(new UserAccounts(account.getAccountNumber(),account.getType(),account.getAmountAvailable()));
			}
			userProfiles.add(new UserProfile(user.getUserId(),user.getUsername(),user.getEmail(),user.getPhone(),user.getAddress(),userAccounts));
		}
		return userProfiles;
	}
	
	public UserEntity getUserById(long userId) {
		UserEntity user = repo.findById(userId).get();
		if(user==null)
			return null;
		return user;
		
	}

	public UserProfile getUserProfile(long userId) {
		UserEntity user = getUserById(userId);
		List<AccountEntity> accountlist = arepo.findAllByUserId(user.getUserId());
		List<UserAccounts> userAccounts = new ArrayList<>();
		for(AccountEntity account: accountlist) {
			userAccounts.add(new UserAccounts(account.getAccountNumber(),account.getType(),account.getAmountAvailable()));
		}
		UserProfile userProfile = new UserProfile();
		userProfile.setUserName(user.getUsername());
		userProfile.setEmail(user.getEmail());
		userProfile.setPhone(user.getPhone());
		userProfile.setAddress(user.getAddress());
		userProfile.setUserAccounts(userAccounts);
		return userProfile;
	}

	public String deletebyUserId(long userId) {
		Optional<UserEntity> user = repo.findById(userId);
		if(user.isPresent()) {
			repo.deleteById(userId);
			arepo.deleteAllByUserId(userId);			
			return "Success";
		}
		else {
			return "Failure";
		}
	}
	
	public UserEntity update(long userId,UserEntity userdata) {
		UserEntity user = repo.findById(userId).get();
		
    	if(user!=null) {
    		user.setUsername(userdata.getUsername());
    		user.setEmail(userdata.getEmail());
    		user.setPhone(userdata.getPhone());
    		user.setRole(userdata.getRole());
    		user.setAddress(userdata.getAddress());
    		
    		user =  repo.save(user);
    		
    	}
		return user;
	}
	
}
