package com.example.usermicroservice.service;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.FixedDepositEntity;

public interface FixedDepositService {
	AccountEntity getAccountByAccountNumber(String accountNumber);
    void updateAccountBalance(String accountNumber, double amount);
    FixedDepositEntity createFixedDeposit(String accountNumber, double principalAmount, double interestRate, int durationInMonths);
    double calculateMaturityAmount(String accountNumber);
}
