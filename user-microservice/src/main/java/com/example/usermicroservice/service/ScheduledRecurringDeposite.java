package com.example.usermicroservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.RecurringDeposite;
import com.example.usermicroservice.repo.RecurringDepositeRepo;

@Component
public class ScheduledRecurringDeposite {
//	@Autowired
//    private RecurringDepositeService rdService;
//	@Autowired
//	RecurringDepositeRepo repo;
//	@Autowired
//	AccountEntity a;
//
//    public ScheduledRecurringDeposite(RecurringDepositeService rdService) {
//        this.rdService = rdService;
//    }
//
//    @Scheduled(fixedRate = 180000) // Runs every 3 minutes
//    public void executeRecurringDeposit() {
//        // Fetch necessary data and execute recurring deposit logic here
//        List<RecurringDeposite> recurringDeposits = repo.findAll(); // Assuming you have a method to fetch all recurring deposits
//        for (RecurringDeposite rd : recurringDeposits) {
//            String accountNumber =Integer.toString((int) a.getAccountId())  ;
//            Long amount = rd.getTotalAmount();
//            Long interestRate = rd.getInterestRate();
//            
//            // Debit the amount from the account balance
//            rdService.updateAccountBalance(accountNumber, amount);
//
//            // Add the amount with interest to the recurring deposit account
//            rdService.createRecurringDeposit(accountNumber,(long) amount * (1 + interestRate / 100), interestRate);
//        }
//    }
}
