package com.example.usermicroservice.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.DepositRequestEntity;
import com.example.usermicroservice.entity.UserEntity;
import com.example.usermicroservice.repo.DepositRequestRepo;
import com.example.usermicroservice.service.AccountServiceImpl;
import com.example.usermicroservice.service.UserService;
import com.example.usermicroservice.utilities.Payment;

//Controller class for Deposit Request handling
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/deposit")
public class DepositController {
//	Deposit Repo autowired to get DepositRepo Object
	@Autowired
	DepositRequestRepo requestRepo;
//	AccountServiceImplementation autowired
	@Autowired 
	AccountServiceImpl aservice;
	
//	User Service Autowired
	@Autowired
	UserService uservice;

	
	//route for getting all the deposit requests
	@GetMapping("/depositRequest")
	public ResponseEntity<List<DepositRequestEntity>> getDepositRequests() {
		//Find List of Deposit Requests who is unapproved
		List<DepositRequestEntity> requests = requestRepo.findAllByIsApprovedFalse();
		if (requests.size() < 0)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		return ResponseEntity.status(HttpStatus.OK).body(requests);

	}

	
	// route for approving the deposit request
//	@PostMapping("/depositRequest/{requestId}")
//	public ResponseEntity<DepositRequestEntity> approveDepositRequest(@PathVariable Long requestId) {
//		
//		//Finding the request object by the request Id
//		Optional<DepositRequestEntity> requestOptional = requestRepo.findById(requestId);
//
//	    if (requestOptional.isEmpty()) {
//	        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
//	    }
//	    
//	    //Getting Object if its there
//	    DepositRequestEntity request = requestOptional.get();
//	    
//	    //Creating Payment object ,so we will get data from front end in that format.
//		Payment obj = new Payment();
//		obj.setAmount(request.getAmount());
//		obj.setFromAccountNumber(request.getUserAccountNumber());
//		obj.setRemark(request.getRemark());
//		obj.setToAccountNumber("");
//		obj.setType(aservice.findTypeByAccountNumber(request.getUserAccountNumber()));
//		
//		//calling deposit method by passing payment object
////		String status = aservice.depositMoney(obj);
//		
//		//if its success then it will send response and set approve to true and update it in database
//		if(status.equals("success")) {			
//			request.isApproved = true;
//			requestRepo.save(request);
//			return ResponseEntity.status(HttpStatus.OK).body(request);
//		}
//		else {
//			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
//		}
//	}
//	
	
	// route for requesting for a deposit
	@PostMapping("/request")
	public ResponseEntity<Void> requestDeposit(@RequestBody Payment paymentObj) {
		//creating new deposit request object 
		DepositRequestEntity depositRequest = new DepositRequestEntity();
		
		//finding user account by account number
		AccountEntity account = aservice.findByAccountNumber(paymentObj.getFromAccountNumber());
		
		//find user object by user id we getting from account obj
		UserEntity user = uservice.getUserById(account.getUserId());
		
		//setting data in deposit object
		depositRequest.setUserId(account.getUserId());
		depositRequest.setUserName(user.getUsername());
		depositRequest.setEmail(user.getEmail());
		depositRequest.setUserAccountNumber(paymentObj.getFromAccountNumber());
		depositRequest.setAmount(paymentObj.getAmount());
		depositRequest.setRemark(paymentObj.getRemark());
		
		double depositAmount = depositRequest.getAmount();
		
		//checking amount if its less than zero or more
		if (depositAmount > 0) {
			//if its more then we will save request object in database with initial approved false
			depositRequest.isApproved = false;
			requestRepo.save(depositRequest);
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

}
