package com.example.usermicroservice.controller;

import java.util.List;

import javax.security.auth.login.AccountException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermicroservice.entity.TransactionEntity;
import com.example.usermicroservice.service.TransactionService;
import com.example.usermicroservice.utilities.Payment;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/transactions")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    // route for transferring funds
    @PostMapping("/transfer")
    public ResponseEntity<TransactionEntity> transferAmount(@RequestBody Payment request) throws AccountException {
        TransactionEntity transaction = transactionService.transferAmount(request);
        return ResponseEntity.ok(transaction);
    }
    
    @GetMapping("/history/{userId}")
    public ResponseEntity<List<TransactionEntity>> getHistoryByAccountNumber(@PathVariable long userId){
    	List<TransactionEntity> allTransaction = transactionService.getHistoryByAccountNumber(userId);
		if(allTransaction!=null) {
			return ResponseEntity.status(HttpStatus.OK).body(allTransaction);
		}
		else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
    }
}

