package com.example.usermicroservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermicroservice.repo.AccountRepo;
import com.example.usermicroservice.repo.UserRepo;

// Admin dashboard data will fetched from here.
@RestController
@RequestMapping("/userresponse")
@CrossOrigin(origins = "http://localhost:4200")
public class AdminDashboardController {

//User Repo autowired to get UserRepo Object
@Autowired
UserRepo userRepo;

//Account Repo autowired to get AccountRepo Object
@Autowired
AccountRepo accountRepo;


//get all the users in the database
@GetMapping("/totalusers")
Long getAllUser() {
return userRepo.count();
}

//Get total accounts in the database
@GetMapping("/totalaccounts")
Long getAllAccount() {
return accountRepo.count();
}



}
