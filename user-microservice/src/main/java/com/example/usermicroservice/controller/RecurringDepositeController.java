package com.example.usermicroservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.service.RDepositeSerImplimentation;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/RD")
public class RecurringDepositeController {
	
	@Autowired
	private RDepositeSerImplimentation rDService;

	// route for getting rd account with account number
    @GetMapping("/account/{accountNumber}")
    public ResponseEntity<AccountEntity> getAccountByAccountNumber(@PathVariable String accountNumber) {
        AccountEntity account = rDService.getAccountByAccountNumber(accountNumber);
        return ResponseEntity.ok(account);
    }
    
    // route for creating RD
    @PostMapping("/recurring-deposit")
    public ResponseEntity<String> createRecurringDeposit(
            @RequestParam String accountNumber,
            @RequestParam Long amount,
            @RequestParam double interestRate
    ) {
        AccountEntity account = rDService.getAccountByAccountNumber(accountNumber);
        if (account.getAmountAvailable() >= amount) {
            rDService.updateAccountBalance(accountNumber, amount);
            rDService.createRecurringDeposit(accountNumber, amount, interestRate);
            return ResponseEntity.ok("Recurring deposit created successfully.");
        } else {
            return ResponseEntity.badRequest().body("Insufficient balance.");
        }
    }

    //get total saving in the RD
    @GetMapping("/total-savings/{userId}")
    public ResponseEntity<Double> getTotalSavings(@PathVariable long userId) {
        double totalSavings = rDService.calculateTotalSavings(userId);
        return ResponseEntity.ok(totalSavings);
    }

    
}
