package com.example.usermicroservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.service.FixedDepositService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class FixedDepositController {
	@Autowired
	 private FixedDepositService fdService;


		//route for getting account by account number
	    @GetMapping("/account/{accountNumber}")
	    public ResponseEntity<AccountEntity> getAccountByAccountNumber(@PathVariable String accountNumber) {
	        AccountEntity account = fdService.getAccountByAccountNumber(accountNumber);
	        return ResponseEntity.ok(account);
	    }
	    // route for creating FD
	    @PostMapping("/fixed-deposit")
	    public ResponseEntity<String> createFixedDeposit(
	            @RequestParam String accountNumber,
	            @RequestParam double principalAmount,
	            @RequestParam double interestRate,
	            @RequestParam int durationInMonths
	    ) {
	        AccountEntity account = fdService.getAccountByAccountNumber(accountNumber);
	        if (account.getAmountAvailable() >= principalAmount) {
	            fdService.updateAccountBalance(accountNumber, principalAmount);
	            fdService.createFixedDeposit(accountNumber, principalAmount, interestRate, durationInMonths);
	            return ResponseEntity.ok("Fixed deposit created successfully.");
	        } else {
	            return ResponseEntity.badRequest().body("Insufficient balance.");
	        }
	    }
	    
	    // route to get the maturity amount for the given loan
	    @GetMapping("/maturity-amount/{accountNumber}")
	    public ResponseEntity<Double> getMaturityAmount(@PathVariable String accountNumber) {
	        double maturityAmount = fdService.calculateMaturityAmount(accountNumber);
	        return ResponseEntity.ok(maturityAmount);
	    }
}
