package com.example.usermicroservice.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.DepositRequestEntity;
import com.example.usermicroservice.entity.LoanAccountEntity;
import com.example.usermicroservice.entity.LoanRequestEntity;
import com.example.usermicroservice.entity.UserEntity;
import com.example.usermicroservice.repo.AccountRepo;
import com.example.usermicroservice.repo.LoanAccountRepo;
import com.example.usermicroservice.repo.LoanRequestRepo;
import com.example.usermicroservice.service.AccountServiceImpl;
import com.example.usermicroservice.service.FDServiceImplimentation;
import com.example.usermicroservice.service.LoanAccountServiceImpl;
import com.example.usermicroservice.service.UserService;
import com.example.usermicroservice.utilities.ACCOUNTTYPE;
import com.example.usermicroservice.utilities.FDRD;
import com.example.usermicroservice.utilities.Loan;
import com.example.usermicroservice.utilities.rdfd;

//Controller for Laon Account Request Handling
@RestController
//CORS policy issue resolved by adding this annotation
@CrossOrigin(origins = "http://localhost:4200")
//root url
@RequestMapping("/loans")
public class LoanAccountController {
	
	//Autowiring multiple services and repo
    @Autowired
    private LoanAccountServiceImpl lservice;
	@Autowired
	AccountServiceImpl aservice;
	@Autowired
	UserService uservice;
	@Autowired
	LoanRequestRepo repo;
	@Autowired
	LoanAccountRepo arepo;
	@Autowired
	AccountRepo accRepo;
	
	@Autowired 
	FDServiceImplimentation fservice;
	
	
	// route for calculating EMI
    @PostMapping("/calculate-emi")
    public ResponseEntity<Double> calculateEMI(@RequestBody LoanAccountEntity loan) {
        double emi = lservice.calculateEMI(loan);
        return ResponseEntity.ok(emi);
    }
    
    // route for calculating monthly installment
    @PostMapping("/monthly-installment")
    public ResponseEntity<MonthlyInstallmentResponse> calculateMonthlyInstallment(@RequestBody rdfd loan) {
    	System.out.println(loan);

        // Get the deposit type
        FDRD depositType = loan.getType();

        // Validate deposit type
        if (depositType == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }

        // Perform the calculation based on deposit type
        double monthlyInstallment = calculateMonthlyInstallmentByType(loan);

        // Check if the calculation is successful
        if (!Double.isNaN(monthlyInstallment) && !Double.isInfinite(monthlyInstallment)) {
            // Create a response object with the calculated monthly installment
            MonthlyInstallmentResponse response = new MonthlyInstallmentResponse(monthlyInstallment);

            // Return the response object
            return ResponseEntity.ok(response);
        } else {
            // Return a bad request response if the calculation fails
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    private double calculateMonthlyInstallmentByType(rdfd deposit) {
    	double interestRate = getInterestRateByType(deposit.getType());
        // Perform the common calculation using the interest rate
        double monthlyInterestRate = (interestRate / 100) / 12;
        int totalMonths = deposit.getTenure();
        // Compound interest formula: P * (1 + r/n)^(nt) - P
        double totalReturns = deposit.getRequestedAmount() *
                ((Math.pow(1 + monthlyInterestRate, totalMonths) - 1) / monthlyInterestRate);

        return totalReturns;
    }

    private double getInterestRateByType(FDRD depositType) {
        // Add logic to fetch interest rates based on deposit type
        switch (depositType) {
            case REGULARFIXED:
                return 4.0;
            case SENIORCITIZEN:
                return 5.0;
            case TAXSAVER:
                return 3.5;
            case REGULARRECURRING:
                return 4.5;
            default:
                // Default case, return 0 for unsupported types
                return 0;
        }
    }

    
    // route for getting total loan requests
    @GetMapping("/loanRequest")
	public ResponseEntity<List<LoanRequestEntity>> LoanRequests() {
		List<LoanRequestEntity> list = repo.findAllByIsApprovedFalse();
		if(list!=null) {
			return ResponseEntity.status(HttpStatus.OK).body(list);
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}

    // route for applying for a loan
    @PostMapping("/request")
	public ResponseEntity<Void> requestLoan(@RequestBody Loan paymentObj) {
    	//Created new LoanRequest obj
    	LoanRequestEntity loanRequest = new LoanRequestEntity();
    	//Fetched the account by passing acount number
    	UserEntity user = uservice.getUserById(paymentObj.getUserId());
    	AccountEntity account = aservice.createUserAccount(user, ACCOUNTTYPE.LOANACCOUNT);
    	if(account==null) {
    		account = aservice.findByUserIdAndType(user.getUserId(), ACCOUNTTYPE.LOANACCOUNT);
    		System.out.println(account.toString());
    		
    	}
		
		loanRequest.setUserId(paymentObj.getUserId());
		loanRequest.setUserName(user.getUsername());
		loanRequest.setEmail(user.getEmail());
		loanRequest.setUserAccountNumber(account.getAccountNumber());
		loanRequest.setType(paymentObj.getType());;
		loanRequest.setAmount(paymentObj.getRequestedAmount());
		loanRequest.setTenure(paymentObj.getTenure());
		
		//Check if user account balance is greater the 30% of loan amount
		if (paymentObj.getRequestedAmount()>0) {
			//if true save loan request and set isapproved to false
			loanRequest.isApproved = false;
			repo.save(loanRequest);
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
    
    
    // route for approving a loan
    @PostMapping("/loanRequest/{requestId}")
	public ResponseEntity<LoanRequestEntity> approveDepositRequest(@PathVariable Long requestId) {
    	//fetching the request based on request ID
		Optional<LoanRequestEntity> requestOptional = repo.findById(requestId);
		
		//if its empty return error
	    if (requestOptional.isEmpty()) {
	        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	    }
	    LoanRequestEntity request = requestOptional.get();
	    AccountEntity account = aservice.findByUserIdAndType(request.getUserId(), ACCOUNTTYPE.LOANACCOUNT);
		LoanAccountEntity loanaccount = new LoanAccountEntity();
		
		loanaccount.setLoanType(request.getType());
		loanaccount.setPeriod(request.getTenure());
		loanaccount.setRequestedAmount(request.getAmount());
		loanaccount.setRoi(15);
		request.isApproved = true;
		loanaccount.setApproved(request.isApproved);
		arepo.save(loanaccount);
		repo.save(request);
		if(account!=null) {
			account.setAmountAvailable(account.getAmountAvailable()+request.getAmount());
			accRepo.save(account);
		}
		return ResponseEntity.status(HttpStatus.OK).body(request);
    }
}

