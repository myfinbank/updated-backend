package com.example.usermicroservice.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.UserAccountRequestEntity;
import com.example.usermicroservice.entity.UserEntity;
import com.example.usermicroservice.repo.UserAccountRequestRepo;
import com.example.usermicroservice.repo.UserRepo;
import com.example.usermicroservice.service.AccountServices;
import com.example.usermicroservice.utilities.ACCOUNTTYPE;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/account-request")
public class UserAccountRequestController {

	@Autowired
	private UserAccountRequestRepo accountRequestRepo;

	@Autowired
	AccountServices service;

	@Autowired
	UserRepo urepo;

	@PostMapping("/create/{arequestId}/{type}")
	public ResponseEntity<UserAccountRequestEntity> approveDepositRequest(@PathVariable Long arequestId,
			@PathVariable ACCOUNTTYPE type) {
		// fetching the request based on request ID
		Optional<UserAccountRequestEntity> requestOptional = accountRequestRepo.findById(arequestId);
		// if its empty return error
		if (requestOptional.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}

		UserAccountRequestEntity request = requestOptional.get();
		UserEntity user = urepo.findById(request.getUserId()).get();

		AccountEntity createdAccount = service.createUserAccount(user, type);
		if (createdAccount != null) {
			request.setApproved(true);
			accountRequestRepo.save(request);
			return ResponseEntity.status(HttpStatus.OK).body(request);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}

	@PostMapping("/update/{arequestId}/{type}")
	public ResponseEntity<UserAccountRequestEntity> updateDepositRequest(@PathVariable Long arequestId,
			@PathVariable ACCOUNTTYPE type) {
		// fetching the request based on request ID
		Optional<UserAccountRequestEntity> requestOptional = accountRequestRepo.findById(arequestId);
		// if its empty return error
		if (requestOptional.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		UserAccountRequestEntity request = requestOptional.get();
		request.setApproved(true);
		accountRequestRepo.save(request);
		return ResponseEntity.status(HttpStatus.OK).body(request);
	}

	@PostMapping("/request/{type}")
	public ResponseEntity<UserAccountRequestEntity> AccountRequest(@PathVariable ACCOUNTTYPE type,
			@RequestBody UserEntity user) {
		// Retrieve the account request by its ID from the repository
		UserAccountRequestEntity accountRequest = new UserAccountRequestEntity();
		accountRequest.setUserId(user.getUserId());
		accountRequest.setUserName(user.getUsername());
		accountRequest.setEmail(user.getEmail());
		accountRequest.setType(type);
		accountRequest.setApproved(false);

		UserAccountRequestEntity accountCreated = accountRequestRepo.save(accountRequest);
		if (accountCreated != null) {
			// Return the account request with a 200 OK status
			return ResponseEntity.ok(accountRequest);
		} else {
			return ResponseEntity.status(HttpStatus.METHOD_FAILURE).build();
		}
	}

	@GetMapping("/all-accounts")
	public ResponseEntity<List<UserAccountRequestEntity>> getAllAccountRequests() {
		// Retrieve the account request by its ID from the repository
		List<UserAccountRequestEntity> accountRequest = accountRequestRepo.findAllByIsApprovedFalse();
		// Check if the account request exists
		if (accountRequest != null) {
			// Return the account request with a 200 OK status
			return ResponseEntity.ok(accountRequest);
		} else {
			// Return a 404 Not Found status if the account request is not found
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
}
