package com.example.usermicroservice.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.UserEntity;
import com.example.usermicroservice.entity.WithdrawRequestEntity;
import com.example.usermicroservice.repo.WithdrawRequestRepo;
import com.example.usermicroservice.service.AccountServiceImpl;
import com.example.usermicroservice.service.UserService;
import com.example.usermicroservice.utilities.Payment;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/withdrawal")
public class WithdrawalController {

	@Autowired
	WithdrawRequestRepo requestRepo;

	@Autowired
	AccountServiceImpl aservice;

	@Autowired
	UserService uservice;

	private static final double MINIMUM_BALANCE = 100.0;

	// route for getting list of withdrawal request
	@GetMapping("/withdrawRequest")
	public ResponseEntity<List<WithdrawRequestEntity>> getWithdrawlReuests() {
		List<WithdrawRequestEntity> requests = requestRepo.findAllByIsApprovedFalse();
		if (requests.size() < 0)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		return ResponseEntity.status(HttpStatus.OK).body(requests);

	}

	// route for getting withdrawal request requestId
	@PostMapping("/withdrawRequest/{requestId}")
	public ResponseEntity<WithdrawRequestEntity> approveWithdrawlReuest(@PathVariable Long requestId) {
		Optional<WithdrawRequestEntity> requestOptional = requestRepo.findById(requestId);

		if (requestOptional.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}

		WithdrawRequestEntity request = requestOptional.get();
		Payment obj = new Payment();
		obj.setAmount(request.getAmount());
		obj.setFromAccountNumber(request.getUserAccountNumber());
		obj.setRemark(request.getRemark());
		obj.setToAccountNumber("");
		obj.setType(aservice.findTypeByAccountNumber(request.getUserAccountNumber()));
		System.out.println(obj.toString());
		String status = aservice.withdrawMoney(obj);
		if (status.equals("success")) {
			request.isApproved = true;
			requestRepo.save(request);
			return ResponseEntity.status(HttpStatus.OK).body(request);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}

	// route for a withdrawal request
	@PostMapping("/request")
	public ResponseEntity<Void> requestWithdrawal(@RequestBody Payment paymentObj) {
		WithdrawRequestEntity withdrawalRequest = new WithdrawRequestEntity();
		AccountEntity account = aservice.findByAccountNumber(paymentObj.getFromAccountNumber());
		UserEntity user = uservice.getUserById(account.getUserId());

		withdrawalRequest.setUserId(account.getUserId());
		withdrawalRequest.setUserName(user.getUsername());
		withdrawalRequest.setEmail(user.getEmail());
		withdrawalRequest.setUserAccountNumber(paymentObj.getFromAccountNumber());
		withdrawalRequest.setAmount(paymentObj.getAmount());
		withdrawalRequest.setRemark(paymentObj.getRemark());
		double withdrawalAmount = withdrawalRequest.getAmount();

		if (withdrawalAmount > 0 && withdrawalAmount <= 10000) {
			// Check if the account has sufficient balance (replace with actual logic)
			double accountBalance = account.getAmountAvailable();

			// Check if the account balance is above the minimum required
			if (accountBalance >= MINIMUM_BALANCE && accountBalance >= withdrawalAmount) {
				withdrawalRequest.isApproved = false;

				requestRepo.save(withdrawalRequest);

				// Return a ResponseEntity with OK status and without a body
				return ResponseEntity.ok().build();
			} else {
				// Return a bad request response if the account balance is insufficient
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
			}
		} else {
			// Return a bad request response if the withdrawal amount is not within limits
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

}
