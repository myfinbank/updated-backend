package com.example.usermicroservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermicroservice.entity.UserEntity;
import com.example.usermicroservice.service.AuthService;
import com.example.usermicroservice.service.UserService;
import com.example.usermicroservice.utilities.LoginInput;
import com.example.usermicroservice.utilities.UserList;
import com.example.usermicroservice.utilities.UserProfile;

import jakarta.transaction.Transactional;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    @Autowired
    private AuthService authService;
    
    @Autowired
    private UserService userService;
    
    //Login function Mapping
    
    @PostMapping("/login")
    public ResponseEntity<UserEntity> login(@RequestBody LoginInput loginInput) {
        UserEntity user = authService.login(loginInput.getUsername(), loginInput.getPassword());
        if (user == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.ok(user);
    }

    // Signup function Mapping
    @PostMapping("/signup")
    public ResponseEntity<UserEntity> signup(@RequestBody UserEntity user) {
        UserEntity createdUser = authService.signup(user.getUsername(), user.getPassword(), user.getEmail(),
                user.getPhone(),
                user.getAddress());
        if (createdUser == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return ResponseEntity.ok(createdUser);
    }
    
    
    // route to get the list for registerd users
    @GetMapping("/userlist")
    public ResponseEntity<List<UserList>> getUserList(){
    	System.out.println("userlist called");
    	List<UserList> usersList = userService.getAllUsers();
    	if(usersList == null) {
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    	}
    	return ResponseEntity.status(HttpStatus.OK).body(usersList);
    }

    // route for getting userProfile
    @GetMapping("/user-profiles")
    public ResponseEntity<List<UserProfile>> getUserProfiles(){
    	List<UserProfile> usersProfileList = userService.getUserProfiles();
    	if(usersProfileList == null) {
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    	}
    	return ResponseEntity.status(HttpStatus.OK).body(usersProfileList);
    }
    
    @GetMapping("/user-profile/{userId}")
    public ResponseEntity<UserProfile> getUserProfile(@PathVariable long userId){
    	UserProfile usersProfile = userService.getUserProfile(userId);
    	if(usersProfile == null) {
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    	}
    	return ResponseEntity.status(HttpStatus.OK).body(usersProfile);
    }
    @Transactional
    @DeleteMapping("/user-delete/{userId}")
    public ResponseEntity<Void> delete(@PathVariable long userId){

    	String status = userService.deletebyUserId(userId);
    	if(status.equals("Success")) {
    		return ResponseEntity.status(HttpStatus.OK).build();
    	}
    	else {    		
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    	}
    }
    @PutMapping("/user-update/{userId}")
    public ResponseEntity<UserEntity> update(@PathVariable long userId,@RequestBody UserEntity userdata){
    	UserEntity user = userService.update(userId, userdata);
    	if(user!=null) {    		
    		return ResponseEntity.status(HttpStatus.OK).body(user);
    	}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

    }
}
