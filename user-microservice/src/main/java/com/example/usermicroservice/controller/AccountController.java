package com.example.usermicroservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.UserEntity;
import com.example.usermicroservice.service.AccountServices;
import com.example.usermicroservice.utilities.ACCOUNTTYPE;
import com.example.usermicroservice.utilities.Payment;

//Controller class for accounts
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/account")
public class AccountController {
	@Autowired
	AccountServices service;

	// route for getting list of account details by userId
	@GetMapping("/details/{userId}")
	public ResponseEntity<List<AccountEntity>> getAccountDetails(@PathVariable long userId) {
		List<AccountEntity> accountList = service.getAllAccounts(userId);
		if (!accountList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK).body(accountList);
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	
	// route for fund transfer
	@PostMapping("/transfer")
	public ResponseEntity<AccountEntity> transfer(@RequestBody Payment paymentObj){
		AccountEntity updatedAccount = service.transferMoney(paymentObj);
		if(updatedAccount!=null) {			
			return service.getUpdatedAccount(updatedAccount, paymentObj);
		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
	
	@PostMapping("/deposit")
	public ResponseEntity<AccountEntity> depositMoney(@RequestBody Payment paymentObj){
		AccountEntity updatedAccount = service.depositMoney(paymentObj);
		if(updatedAccount!=null) {			
			return ResponseEntity.status(HttpStatus.OK).body(updatedAccount);
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();	
	}
	
}