package com.example.usermicroservice.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class WithdrawRequestEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long wRequestId;
	private long userId;
	private String userName;
	private String email;
	private String userAccountNumber;
	private double amount;
	private String remark;
	public boolean isApproved;
	
	public WithdrawRequestEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WithdrawRequestEntity(long wRequestId, long userId, String userName, String email, String userAccountNumber,
			double amount, String remark, boolean isApproved) {
		super();
		this.wRequestId = wRequestId;
		this.userId = userId;
		this.userName = userName;
		this.email = email;
		this.userAccountNumber = userAccountNumber;
		this.amount = amount;
		this.remark = remark;
		this.isApproved = isApproved;
	}

	public long getwRequestId() {
		return wRequestId;
	}

	public void setwRequestId(long wRequestId) {
		this.wRequestId = wRequestId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserAccountNumber() {
		return userAccountNumber;
	}

	public void setUserAccountNumber(String userAccountNumber) {
		this.userAccountNumber = userAccountNumber;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public boolean isApproved() {
		return isApproved;
	}

	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}

	@Override
	public String toString() {
		return "WithdrawRequestEntity [wRequestId=" + wRequestId + ", userId=" + userId + ", userName=" + userName
				+ ", email=" + email + ", userAccountNumber=" + userAccountNumber + ", amount=" + amount + ", remark="
				+ remark + ", isApproved=" + isApproved + "]";
	}
	
}
