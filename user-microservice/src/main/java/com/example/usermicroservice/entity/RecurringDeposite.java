package com.example.usermicroservice.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class RecurringDeposite{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int depositeId;
	private int userId;
	private int period;
	private long totalAmount;
	private Long installment;
	private Long interestRate;
	
	
	public RecurringDeposite(String accountNumber, double amount, double interestRate2) {
		super();
		// TODO Auto-generated constructor stub
	}
	public RecurringDeposite(int depositeId, int userId, int period, long totalAmount, Long installment,
			Long interestRate) {
		super();
		this.depositeId = depositeId;
		this.userId = userId;
		this.period = period;
		this.totalAmount = totalAmount;
		this.installment = installment;
		this.interestRate = interestRate;
	}
	public int getDepositeId() {
		return depositeId;
	}
	public void setDepositeId(int depositeId) {
		this.depositeId = depositeId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getPeriod() {
		return period;
	}
	public void setPeriod(int period) {
		this.period = period;
	}
	public long getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(long totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Long getInstallment() {
		return installment;
	}
	public void setInstallment(Long installment) {
		this.installment = installment;
	}
	public Long getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(Long interestRate) {
		this.interestRate = interestRate;
	}
	 
}
