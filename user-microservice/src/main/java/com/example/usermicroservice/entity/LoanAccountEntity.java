package com.example.usermicroservice.entity;

import com.example.usermicroservice.utilities.LOANTYPE;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;

@Entity
public class LoanAccountEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int loanId;
	private double requestedAmount;
	private int interestAmount;
	private int roi;
	private int period;
	@Enumerated(EnumType.STRING)
	private LOANTYPE loanType;
	private boolean isApproved;
	
	
	public LoanAccountEntity() {
		super();
		// TODO Auto-generated constructor stub
	}


	public LoanAccountEntity(int loanId, double requestedAmount, int interestAmount, int roi,
			int period, LOANTYPE loanType, boolean isApproved) {
		super();
		this.loanId = loanId;
		this.requestedAmount = requestedAmount;
		this.interestAmount = interestAmount;
		this.roi = roi;
		this.period = period;
		this.loanType = loanType;
		this.isApproved = isApproved;
	}


	public int getLoanId() {
		return loanId;
	}


	public void setLoanId(int loanId) {
		this.loanId = loanId;
	}


	public double getRequestedAmount() {
		return requestedAmount;
	}


	public void setRequestedAmount(double requestedAmount) {
		this.requestedAmount = requestedAmount;
	}


	public int getInterestAmount() {
		return interestAmount;
	}


	public void setInterestAmount(int interestAmount) {
		this.interestAmount = interestAmount;
	}


	public int getRoi() {
		return roi;
	}


	public void setRoi(int roi) {
		this.roi = roi;
	}


	public int getPeriod() {
		return period;
	}


	public void setPeriod(int period) {
		this.period = period;
	}


	public LOANTYPE getLoanType() {
		return loanType;
	}


	public void setLoanType(LOANTYPE loanType) {
		this.loanType = loanType;
	}


	public boolean isApproved() {
		return isApproved;
	}


	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}


	@Override
	public String toString() {
		return "LoanAccountEntity [loanId=" + loanId + ", requestedAmount=" + requestedAmount + ", interestAmount="
				+ interestAmount + ", roi=" + roi + ", period=" + period
				+ ", loanType=" + loanType + ", isApproved=" + isApproved + "]";
	}


	
	
}
