package com.example.usermicroservice.entity;

import com.example.usermicroservice.utilities.ACCOUNTTYPE;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class UserAccountRequestEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long aRequestId;
	public long userId; 
	public String userName;
	public String email;
	@Enumerated(EnumType.STRING)
	private ACCOUNTTYPE type;
	private boolean isApproved; 

	public UserAccountRequestEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserAccountRequestEntity(Long aRequestId, long userId, String userName, String email, ACCOUNTTYPE type,
			boolean isApproved) {
		super();
		this.aRequestId = aRequestId;
		this.userId = userId;
		this.userName = userName;
		this.email = email;
		this.type = type;
		this.isApproved = isApproved;
	}

	public Long getaRequestId() {
		return aRequestId;
	}

	public void setaRequestId(Long aRequestId) {
		this.aRequestId = aRequestId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public ACCOUNTTYPE getType() {
		return type;
	}

	public void setType(ACCOUNTTYPE type) {
		this.type = type;
	}

	public boolean isApproved() {
		return isApproved;
	}

	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}

	@Override
	public String toString() {
		return "UserAccountRequestEntity [aRequestId=" + aRequestId + ", userId=" + userId + ", userName=" + userName
				+ ", email=" + email + ", type=" + type + ", isApproved=" + isApproved + "]";
	}

	
	
}