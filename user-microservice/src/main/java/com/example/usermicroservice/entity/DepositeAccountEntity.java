package com.example.usermicroservice.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class DepositeAccountEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int investedId;
	private int period;
	private long investedAmount;
	private int installment;
	public int getInvestedId() {
		return investedId;
	}
	public void setInvestedId(int investedId) {
		this.investedId = investedId;
	}
	public int getPeriod() {
		return period;
	}
	public void setPeriod(int period) {
		this.period = period;
	}
	public long getInvestedAmount() {
		return investedAmount;
	}
	public void setInvestedAmount(long investedAmount) {
		this.investedAmount = investedAmount;
	}
	public int getInstallment() {
		return installment;
	}
	public void setInstallment(int installment) {
		this.installment = installment;
	}
	
	 
}
