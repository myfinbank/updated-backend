package com.example.usermicroservice.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class FixedDepositEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int fixedDepositId;
	private long userId;
	private String accountNumber;
    private double principalAmount;
    private double interestRate;
    private int durationInMonths;
    
	public int getFixedDepositId() {
		return fixedDepositId;
	}
	public void setFixedDepositId(int fixedDepositId) {
		this.fixedDepositId = fixedDepositId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public double getPrincipalAmount() {
		return principalAmount;
	}
	public void setPrincipalAmount(double principalAmount) {
		this.principalAmount = principalAmount;
	}
	public double getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}
	public int getDurationInMonths() {
		return durationInMonths;
	}
	public void setDurationInMonths(int durationInMonths) {
		this.durationInMonths = durationInMonths;
	}
	public FixedDepositEntity(int fixedDepositId, long userId, String accountNumber,
			double principalAmount, double interestRate, int durationInMonths) {
		super();
		this.fixedDepositId = fixedDepositId;
		this.userId = userId;
		this.accountNumber = accountNumber;
		this.principalAmount = principalAmount;
		this.interestRate = interestRate;
		this.durationInMonths = durationInMonths;
	}
	public FixedDepositEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public FixedDepositEntity(String accountNumber2, double principalAmount2, double interestRate2,
			int durationInMonths2) {
		// TODO Auto-generated constructor stub
	}
    
	
	
}
