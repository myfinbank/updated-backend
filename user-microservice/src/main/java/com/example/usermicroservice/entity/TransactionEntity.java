package com.example.usermicroservice.entity;

import java.util.Date;

import com.example.usermicroservice.utilities.ACCOUNTTYPE;
import com.example.usermicroservice.utilities.STATUS;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class TransactionEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	long id;
	long userId;
	String transactionId;
	Date timeStamp;
	String transactorAccountId;
	String transacteeAccountId;
	String remark;
	double amount;
	@Enumerated(EnumType.STRING)
	private ACCOUNTTYPE type;
	@Enumerated(EnumType.STRING)
	private STATUS status;
	
	public TransactionEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TransactionEntity(long id, long userId, String transactionId, Date timeStamp, String transactorAccountId,
			String transacteeAccountId, String remark, double amount, ACCOUNTTYPE type, STATUS status) {
		super();
		this.id = id;
		this.userId = userId;
		this.transactionId = transactionId;
		this.timeStamp = timeStamp;
		this.transactorAccountId = transactorAccountId;
		this.transacteeAccountId = transacteeAccountId;
		this.remark = remark;
		this.amount = amount;
		this.type = type;
		this.status = status;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getTransactorAccountId() {
		return transactorAccountId;
	}

	public void setTransactorAccountId(String transactorAccountId) {
		this.transactorAccountId = transactorAccountId;
	}

	public String getTransacteeAccountId() {
		return transacteeAccountId;
	}

	public void setTransacteeAccountId(String transacteeAccountId) {
		this.transacteeAccountId = transacteeAccountId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public ACCOUNTTYPE getType() {
		return type;
	}

	public void setType(ACCOUNTTYPE type) {
		this.type = type;
	}

	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "TransactionEntity [id=" + id + ", userId=" + userId + ", transactionId=" + transactionId
				+ ", timeStamp=" + timeStamp + ", transactorAccountId=" + transactorAccountId + ", transacteeAccountId="
				+ transacteeAccountId + ", remark=" + remark + ", amount=" + amount + ", type=" + type + ", status="
				+ status + "]";
	}

	
	
		
}
