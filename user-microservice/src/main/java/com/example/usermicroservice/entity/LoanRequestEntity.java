package com.example.usermicroservice.entity;

import com.example.usermicroservice.utilities.LOANTYPE;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class LoanRequestEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long lRequestId;
	private long userId;
	private String userName;
	private String email;
	private String userAccountNumber;
	private double amount;
	private int tenure;
	@Enumerated(EnumType.STRING)
	private LOANTYPE type;
	public boolean isApproved;
	public LoanRequestEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public LoanRequestEntity(Long lRequestId, long userId, String userName, String email, String userAccountNumber,
			double amount,int tenure, LOANTYPE type, boolean isApproved) {
		super();
		this.lRequestId = lRequestId;
		this.userId = userId;
		this.userName = userName;
		this.email = email;
		this.userAccountNumber = userAccountNumber;
		this.amount = amount;
		this.tenure = tenure;
		this.type = type;
		this.isApproved = isApproved;
	}
	public Long getlRequestId() {
		return lRequestId;
	}
	public void setlRequestId(Long lRequestId) {
		this.lRequestId = lRequestId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserAccountNumber() {
		return userAccountNumber;
	}
	public void setUserAccountNumber(String userAccountNumber) {
		this.userAccountNumber = userAccountNumber;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public int getTenure() {
		return tenure;
	}
	public void setTenure(int tenure) {
		this.tenure = tenure;
	}
	public LOANTYPE getType() {
		return type;
	}
	public void setType(LOANTYPE type) {
		this.type = type;
	}
	public boolean isApproved() {
		return isApproved;
	}
	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}
	@Override
	public String toString() {
		return "LoanRequestEntity [lRequestId=" + lRequestId + ", userId=" + userId + ", userName=" + userName
				+ ", email=" + email + ", userAccountNumber=" + userAccountNumber + ", amount=" + amount + ", tenure="
				+ tenure + ", type=" + type + ", isApproved=" + isApproved + "]";
	}
	
	
}
