package com.example.usermicroservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

import com.example.usermicroservice.entity.AccountEntity;
import com.example.usermicroservice.entity.UserEntity;
import com.example.usermicroservice.repo.AccountRepo;
import com.example.usermicroservice.repo.UserRepo;
import com.example.usermicroservice.service.UserService;
import com.example.usermicroservice.utilities.ACCOUNTTYPE;
import com.example.usermicroservice.utilities.ROLE;
import com.example.usermicroservice.utilities.UserList;
import com.example.usermicroservice.utilities.UserProfile;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

	    @Mock
	    private UserRepo userRepo;

	    @Mock
	    private AccountRepo accountRepo;

	    @InjectMocks
	    private UserService userService;

	    private UserEntity userEntity;
	    private AccountEntity accountEntity;

	    @Before
	    public void setup() {
	        userEntity = new UserEntity(1L,"aman", "aman@gmail.com","12341234", 7828737234L ,ROLE.CUS, "bhopal");
	        accountEntity = new AccountEntity("788899922277", 1L, 100L, ACCOUNTTYPE.SAVING);
	    }

	    @Test
	    public void testGetAllUsers() {
	        List<UserEntity> userList = new ArrayList<>();
	        userList.add(userEntity);
	        when(userRepo.findAll()).thenReturn(userList);

	        List<UserList> users = userService.getAllUsers();

	        assertEquals(1, users.size());
	        assertEquals("aman", users.get(0).getUserName());
	    }

	    @Test
	    public void testGetUserProfiles() {
	        List<UserEntity> userList = new ArrayList<>();
	        userList.add(userEntity);
	        when(userRepo.findAll()).thenReturn(userList);
	        when(accountRepo.findAllByUserId(1L)).thenReturn(List.of(accountEntity));

	        List<UserProfile> userProfiles = userService.getUserProfiles();

	        assertEquals(1, userProfiles.size());
	        assertEquals("aman", userProfiles.get(0).getUserName());
	        assertEquals(1, userProfiles.get(0).getUserAccounts().size());
	    }

	    @Test
	    public void testGetUserById() {
	        when(userRepo.findById(1L)).thenReturn(Optional.of(userEntity));

	        UserEntity user = userService.getUserById(1L);

	        assertNotNull(user);
	        assertEquals("aman", user.getUsername());
	    }

	    @Test
	    public void testGetUserProfile() {
	        when(userRepo.findById(1L)).thenReturn(Optional.of(userEntity));
	        when(accountRepo.findAllByUserId(1L)).thenReturn(List.of(accountEntity));

	        UserProfile userProfile = userService.getUserProfile(1L);

	        assertEquals("aman", userProfile.getUserName());
	        assertEquals("aman@gmail.com", userProfile.getEmail());
	        assertEquals(7828737234L, userProfile.getPhone());
	        assertEquals("bhopal", userProfile.getAddress());
	        assertEquals(1, userProfile.getUserAccounts().size());
	    }

	    @Test
	    public void testDeletebyUserId() {
	        when(userRepo.findById(1L)).thenReturn(Optional.of(userEntity));
	        doNothing().when(accountRepo).deleteAllByUserId(1L);

	        String result = userService.deletebyUserId(1L);

	        assertEquals("Success", result);
	    }

	    @Test
	    public void testUpdate() {
	        when(userRepo.findById(1L)).thenReturn(Optional.of(userEntity));
	        when(userRepo.save(any(UserEntity.class))).thenReturn(userEntity);

	        UserEntity updatedUser = new UserEntity(1L, "mayank", "mayank@gmail.com", "12341234", 7828737235L,ROLE.AD ,"Banglore");
	        UserEntity result = userService.update(1L, updatedUser);

	        assertEquals("mayank", result.getUsername());
	        assertEquals("mayank@gmail.com", result.getEmail());
	        assertEquals(7828737235L, result.getPhone());
	        assertEquals("Banglore", result.getAddress());
	    }
	}

